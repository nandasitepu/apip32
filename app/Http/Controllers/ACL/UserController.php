<?php

namespace App\Http\Controllers\ACL;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\Model\User;
use App\Model\Role;


class UserController extends Controller
{
    
    // USER

    public function roles() 
    {
      $users = User::all();
      return view ('users.roles')->with('users', $users);
    }


    public function profil() 
      {
        $users = User::all();
        return view ('users.profil')->with('users', $users);
      }


    // ADMIN
    public function index() 
      {
        $users = User::all();
        $roles = Role::all();
        return view ('admin.manage-users')->with('users', $users)->with('roles', $roles);
      }

    public function create() 
      {
        return view ('users.new');
      }

    public function update(Request $request)
    {
      $user = User::where('email', $request['email'])->first();
      
      $user->roles()->detach();
      
      if($request['role_admin']) {
        $user->roles()->attach(Role::where('nama', 'admin')->first() );
      }
      if($request['role_user']) {
        $user->roles()->attach(Role::where('nama', 'user')->first() );
      }
      if($request['role_apip']) {
        $user->roles()->attach(Role::where('nama', 'apip')->first() );
      }
      if($request['role_bpkp_pusat']) {
        $user->roles()->attach(Role::where('nama', 'bpkp_pusat')->first() );
      }
      if($request['role_bpkp_sulbar']) {
        $user->roles()->attach(Role::where('nama', 'bpkp_sulbar')->first() );
      }
   
      return redirect()->back();
    }
}

