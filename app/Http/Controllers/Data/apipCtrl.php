<?php

namespace App\Http\Controllers\Data;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\APIP\APIP;
use App\Model\APIP\APIP_Data;
use App\Model\APIP\APIP_Elemen;
use App\Model\APIP\APIP_Kapabilitas;
use App\Model\APIP\APIP_Level;
use App\Model\APIP\APIP_Implementasi;
use App\Model\APIP\APIP_KPA;


class apipCtrl extends Controller
{
    // JSON All APIP
    public function getAPIP ()
    {
      $apip = APIP_Data::all();

      return response()->json($apip);
    }

    // Index Page
    public function index() 
    {
        return view('apip.index'); 
    }


    public function show_data_apip_detail ($id) 
    {
        $apip     = APIP_Data::find($id);
        // $pegawai  = 
        // $keuangan =

        // get previous user id
        $previous = APIP_Data::where('id', '<', $apip->id)->max('id');

        // get next user id
        $next = APIP_DATA::where('id', '>', $apip->id)->min('id');
        return view ('apip.detail') ->with('previous', $previous)
                                    ->with('next', $next)
                                    ->with('apip' , $apip);
    }

    // Show Data Elemen Inspektorat
    public function show_data_apip($id)
    {
       
        
        $apip   = APIP_Data::find($id);
        $kpa    = APIP_KPA::all();
    // Condition Elemen
        $condition    = ['apip_id' => $id];
        $condition1   = ['apip_id' => $id, 'apip_elemen_id' => '1'];
        $condition2   = ['apip_id' => $id, 'apip_elemen_id' => '2'];
        $condition3   = ['apip_id' => $id, 'apip_elemen_id' => '3'];
        $condition4   = ['apip_id' => $id, 'apip_elemen_id' => '4'];
        $condition5   = ['apip_id' => $id, 'apip_elemen_id' => '5'];
        $condition6   = ['apip_id' => $id, 'apip_elemen_id' => '6'];
    // Condition KPA
        $c_1    = ['apip_id' => $id, 'apip_kpa_id' => '1'];
        $c_2    = ['apip_id' => $id, 'apip_kpa_id' => '2'];
        $c_3    = ['apip_id' => $id, 'apip_kpa_id' => '3'];
        $c_4    = ['apip_id' => $id, 'apip_kpa_id' => '4'];
        $c_5    = ['apip_id' => $id, 'apip_kpa_id' => '5'];
        $c_6    = ['apip_id' => $id, 'apip_kpa_id' => '6'];
        $c_7    = ['apip_id' => $id, 'apip_kpa_id' => '7'];
        $c_8    = ['apip_id' => $id, 'apip_kpa_id' => '8'];
        $c_9    = ['apip_id' => $id, 'apip_kpa_id' => '9'];
        $c_10   = ['apip_id' => $id, 'apip_kpa_id' => '10'];
        $c_11   = ['apip_id' => $id, 'apip_kpa_id' => '11'];
        $c_12   = ['apip_id' => $id, 'apip_kpa_id' => '12'];
        $c_13   = ['apip_id' => $id, 'apip_kpa_id' => '13'];
        $c_14   = ['apip_id' => $id, 'apip_kpa_id' => '14'];
        $c_15   = ['apip_id' => $id, 'apip_kpa_id' => '15'];
        $c_16   = ['apip_id' => $id, 'apip_kpa_id' => '16'];
        $c_17   = ['apip_id' => $id, 'apip_kpa_id' => '17'];
        $c_18   = ['apip_id' => $id, 'apip_kpa_id' => '18'];
        $c_19   = ['apip_id' => $id, 'apip_kpa_id' => '19'];
        $c_20   = ['apip_id' => $id, 'apip_kpa_id' => '20'];
        $c_21   = ['apip_id' => $id, 'apip_kpa_id' => '21'];
        $c_22   = ['apip_id' => $id, 'apip_kpa_id' => '22'];
        $c_23   = ['apip_id' => $id, 'apip_kpa_id' => '23'];
        $c_24   = ['apip_id' => $id, 'apip_kpa_id' => '24'];

    // Conditional Elemen
        $all            = APIP_Implementasi::where($condition) ->get();
        $elemen1        = APIP_Implementasi::where($condition1)->get();
        $elemen2        = APIP_Implementasi::where($condition2)->get();
        $elemen3        = APIP_Implementasi::where($condition3)->get();
        $elemen4        = APIP_Implementasi::where($condition4)->get();
        $elemen5        = APIP_Implementasi::where($condition5)->get();
        $elemen6        = APIP_Implementasi::where($condition6)->get();

    // Conditional KPA
        $kpa1         = APIP_Implementasi::where($c_1)->get();
        $kpa2         = APIP_Implementasi::where($c_2)->get();
        $kpa3         = APIP_Implementasi::where($c_3)->get();
        $kpa4         = APIP_Implementasi::where($c_4)->get();
        $kpa5         = APIP_Implementasi::where($c_5)->get();
        $kpa6         = APIP_Implementasi::where($c_6)->get();
        $kpa7         = APIP_Implementasi::where($c_7)->get();
        $kpa8         = APIP_Implementasi::where($c_8)->get();
        $kpa9         = APIP_Implementasi::where($c_9)->get();
        $kpa10        = APIP_Implementasi::where($c_10)->get();
        $kpa11        = APIP_Implementasi::where($c_11)->get();
        $kpa12        = APIP_Implementasi::where($c_12)->get();  
        $kpa13        = APIP_Implementasi::where($c_13)->get();
        $kpa14        = APIP_Implementasi::where($c_14)->get();
        $kpa15        = APIP_Implementasi::where($c_15)->get();
        $kpa16        = APIP_Implementasi::where($c_16)->get();
        $kpa17        = APIP_Implementasi::where($c_17)->get();
        $kpa18        = APIP_Implementasi::where($c_18)->get();  
        $kpa19        = APIP_Implementasi::where($c_19)->get();
        $kpa20        = APIP_Implementasi::where($c_20)->get();
        $kpa21        = APIP_Implementasi::where($c_21)->get();
        $kpa22        = APIP_Implementasi::where($c_22)->get();
        $kpa23        = APIP_Implementasi::where($c_23)->get();
        $kpa24        = APIP_Implementasi::where($c_24)->get();
        
        $chart   = collect($apip)->only       ( 'peran_dan_layanan', 
                                        'manajemen_sdm', 
                                        'praktik_profesional', 
                                        'manajemen_kinerja_dan_akuntabilitas',
                                        'hubungan_dan_budaya_organisasi',
                                        'struktur_tata_kelola');
        

      
        // get previous user id
        $previous = APIP_Data::where('id', '<', $apip->id)->max('id');

        // get next user id
        $next = APIP_DATA::where('id', '>', $apip->id)->min('id');

        return view ('apip.data')       ->with('apip' , $apip)
                                        ->with('kpa' , $kpa)
                                        ->with('all', $all)
                                        ->with('previous', $previous)
                                        ->with('next', $next)
                                        ->with('elemen1', $elemen1)
                                        ->with('elemen2', $elemen2)
                                        ->with('elemen3', $elemen3)
                                        ->with('elemen4', $elemen4)
                                        ->with('elemen5', $elemen5)
                                        ->with('elemen6', $elemen6)
                                        ->with('kpa1', $kpa1)
                                        ->with('kpa2', $kpa2)
                                        ->with('kpa3', $kpa3)
                                        ->with('kpa4', $kpa4)
                                        ->with('kpa5', $kpa5)
                                        ->with('kpa6', $kpa6)
                                        ->with('kpa7', $kpa7)
                                        ->with('kpa8', $kpa8)
                                        ->with('kpa9', $kpa9)
                                        ->with('kpa10', $kpa10)
                                        ->with('kpa11', $kpa11)
                                        ->with('kpa12', $kpa12)
                                        ->with('kpa13', $kpa13)
                                        ->with('kpa14', $kpa14)
                                        ->with('kpa15', $kpa15)
                                        ->with('kpa16', $kpa16)
                                        ->with('kpa17', $kpa17)
                                        ->with('kpa18', $kpa18)
                                        ->with('kpa19', $kpa19)
                                        ->with('kpa20', $kpa20)
                                        ->with('kpa21', $kpa21)
                                        ->with('kpa22', $kpa22)
                                        ->with('kpa23', $kpa23)
                                        ->with('kpa24', $kpa24)
                                        ->with('chart',json_encode($chart));
                                       
    }

    // Edit Data Umum Inspektorat
    public function edit_data_umum($id)
    {
        $apip  = APIP_Data::find($id);
        return view ('app.apip.form.data-umum')->with('apip',$apip);
    }

    // Edit Data Kapabilitas Inspektorat
    public function edit_data_kapabilitas($id)
    {
        $apip  = APIP_Data::find($id);
        return view ('app.apip.form.data-level')->with('apip',$apip);
    }

    // Edit Data Implementasi Inspektorat
    public function edit_data_implementasi($id)
    {
        $apip  = APIP_Implementasi::find($id);
        return view ('app.apip.form.data-level')->with('apip',$apip);
    }

    // Upload Foto
    public function upload_foto($id) 
    {
        $this->validate($request, [
            'foto' => 'required|image|mimes:jpeg,png,gif,jpg|max:2048'
        ]);
    }

    // Upload Logo
    public function upload_logo($id) 
    {
        $this->validate($request, [
            'gambar' => 'required|image|mimes:jpeg,png,gif,jpg|max:2048'
        ]);
    }

    // Update Data APIP
    public function update_data(Request $request, $id)
    {
        $this->validate($request, [
            'pimpinan' => 'required',
            'kontak' => 'required',
        ]);
  
        $apip = APIP_Data::find($id)->update( $request->all() );
  
        // Alert Success Message
        Alert::success('Update Sukses!')->persistent("Tutup");
  
        // Back to The Index Page
        return back();
    }

     // Update Kpaabilita APIP


    public function destroy($id)
    {
        //
    }
}
