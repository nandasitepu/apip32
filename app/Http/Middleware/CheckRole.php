<?php
namespace App\Http\Middleware;
use Closure;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        if(!auth()->check()) {
            return redirect('login')->withErrors('Harap Login'); 
        }
        
        if (! $request->user()->hasRole($role)) {
            abort(401, 'This action is unauthorized.');
        }
        return $next($request);

        // if(!auth()->check()) {
        //     return redirect('login')->withErrors('Harap Login'); 
        // }

        // if (auth()->user()->hasAnyRole($role)) {
        //     return $next($request);
        // }

        // return redirect()->route('errors.401')->withErrors('Hak Akses Tidak Cukup'); */
      

        // if ($request->user() === null) {
        //     return redirect('login')->withErrors(['Harus Login']); // Redirect Login Page Harus Login
        // }

        // $actions = $request->route()->getAction();
        // $roles = isset($actions['roles']) ? $actions['roles'] : null;

        // if ($request->user()->hasAnyRole($roles) || !$roles) {
        //     return $next($request);
        // }
        // return redirect('pegawai')->withErrors(['Hak Akses Kurang']); // Make View For Hak Akses Tidak Cukup */
    }
}