<?php

namespace App\Model\APIP;

use Illuminate\Database\Eloquent\Model;

class APIP extends Model
{
  protected $table = 'apip';
  public $fillable = [
    'pernyataan', 'dokumen', 'catatan',  'status'
  ];

  public function apip_data()
  {
    return $this->belongsTo(APIP_Data::class, 'apip_data_id');
  }

  public function apip_elemen()
  {
    return $this->belongsTo(APIP_Elemen::class, 'apip_elemen_id');
  }

  public function apip_level()
  {
    return $this->belongsTo(APIP_Level::class, 'apip_level_id');
  }

  public function apip_kapabilitas()
  {
    return $this->belongsTo(APIP_Kapabilitas::class, 'apip_kapabilitas_id');
  }

  public function apip_kpa()
  {
    return $this->belongsTo(APIP_KPA::class, 'apip_kpa_id');
  }

  public function apip_implementasi()
  {
    return $this->belongsTo(APIP_Implementasi::class, 'apip_implementasi_id');
  }
}
