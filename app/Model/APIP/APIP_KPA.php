<?php

namespace App\Model\APIP;

use Illuminate\Database\Eloquent\Model;

class APIP_KPA extends Model
{
    protected $table = 'apip_kpa';

    public function apip()
    {
    return $this->belongsTo(APIP::class);
    }
}
