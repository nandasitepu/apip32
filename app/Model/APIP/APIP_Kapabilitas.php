<?php

namespace App\Model\APIP;

use Illuminate\Database\Eloquent\Model;

class APIP_Kapabilitas extends Model
{
    protected $table = 'apip_kapabilitas';
    
    public function apip()
    {
    return $this->belongsTo(APIP::class);
    }
}
