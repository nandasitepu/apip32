<?php

namespace App\Model\APIP;

use Illuminate\Database\Eloquent\Model;

class APIP_Pernyataan extends Model
{
  public function apip()
  {
    return $this->belongsTo(APIP::class);
  }
}
