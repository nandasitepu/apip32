<?php

namespace App\Model\APIP;

use Illuminate\Database\Eloquent\Model;

class APIP_Elemen extends Model
{
    protected $table = 'apip_elemen';

    public function apip()
    {
    return $this->belongsTo(APIP::class);
    }
}
