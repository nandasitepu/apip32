<?php

namespace App\Model\APIP;

use Illuminate\Database\Eloquent\Model;

class APIP_Level extends Model
{
    protected $table = 'apip_level';
    
    public function apip()
    {
    return $this->belongsTo(APIP::class);
    }
}
