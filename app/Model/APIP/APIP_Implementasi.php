<?php

namespace App\Model\APIP;

use Illuminate\Database\Eloquent\Model;

class APIP_Implementasi extends Model
{
    protected $table = 'apip_implementasi';

    public function apip()
    {
    return $this->belongsTo(APIP::class);
    }
}
