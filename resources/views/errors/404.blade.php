@extends('_l.main') 
@section('title') 
    MEDIA KOMUNIKASI (MAKSI) - APIP SULAWESI BARAT

@endsection

@section('content')
<div class="title text-center" style="min-height:350px">
  
   <div class="panel panel-default" style=";margin-top:100px">
      <div class="panel-body">
              <hr>
              <p style="font-size:18px" class="label label-default">Halaman Tidak Ditemukan</p>
              <br>
              <h1 > Error 404</h1>
              <br> 
              <p style="font-size:14px" class="label label-info">Silahkan Cari Halaman Lain</p>
              <hr>
              <a href="{{route('welcome')}}">
                <button type="button" name="button" class="btn btn-primary btn-sm cool">
                  <i class="fa fa-home fa-2x"></i>
                </button>
              </a>
              <a href="#">
                <button type="button" name="button" class="btn btn-info btn-sm cool">
                  <i class="fa fa-dashcube fa-2x"></i>
                </button>
              </a>
      </div>
   </div>
</div>
@endsection


