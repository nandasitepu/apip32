@extends('_l.main') 
@section('title') 
    MEDIA KOMUNIKASI (MAKSI) - APIP SULAWESI BARAT

@endsection

@section('content')
<div class="title text-center">
    <h1 > Error 401</h1>
    <hr>
    <p style="font-size:18px" class="label label-default">HAK AKSES ANDA TIDAK CUKUP</p>
    <br>
   
    <br> 
    <p style="font-size:14px" class="label label-info">Silahkan Cari Halaman Lain</p>
    <hr>
    <a href="#">
      <button type="button" name="button" class="btn btn-primary btn-sm cool">
        <i class="fa fa-home fa-2x"></i>
      </button>
    </a>
    <a href="#">
      <button type="button" name="button" class="btn btn-info btn-sm cool">
        <i class="fa fa-dashcube fa-2x"></i>
      </button>
    </a>
  </div>
@endsection


