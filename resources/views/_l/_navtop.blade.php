<link href="https://fonts.googleapis.com/css?family=Bungee+Shade" rel="stylesheet">
<nav class="navbar navbar-default navbar-static-top">
    <div class="navbar-header">
        <!-- Collapsed Hamburger -->
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
            <span class="sr-only">Toggle Navigation</span>
            <i class="fa fa-maxcdn fa-lg" aria-hidden="true"></i>
        </button>


        <!-- Branding Image -->
        <a class="navbar-brand " href="{{ url('/') }}">
            <span>
                 <h4>
                    <b>                
                         <i class="fa fa-maxcdn fa-3x text-primary" aria-hidden="true"></i> 
                         &nbsp;
                         <span style="font-family: 'Bungee Shade', cursive; font-size:36px">AKSI</span>
                    </b>
                 </h4>
            </span>
        </a>
    </div>

    <div class="collapse navbar-collapse" id="app-navbar-collapse">
        <!-- Right Side Of Navbar -->
        <ul class="visible-xs list-unstyled" style="margin-top:15px">
            <!-- Authentication Links -->
            @if (Auth::guest())
                <button type="button" class="btn btn-default btn-block btn-sm">Profil APIP</button>
                <button type="button" class="btn btn-default btn-block btn-sm">Pemerintahan Daerah</button>
                <button type="button" class="btn btn-default btn-block btn-sm">Data</button>
            @else
                <button type="button" class="btn btn-default btn-sm">Selamat Datang, {{ Auth::user()->name }}</button>
                <button type="button" class="btn btn-default btn-sm">Profil APIP</button>
                <button type="button" class="btn btn-default btn-sm">Pemerintahan Daerah</button>
                <button type="button" class="btn btn-default btn-sm">Data</button>
            @endif
        </ul>
        <ul class="hidden-xs pull-right" style="margin-top:15px">
            <!-- Authentication Links -->
            @if (Auth::guest())
            
                <li class="btn-group">
                    <button type="button" class="btn btn-default btn-sm">
                        <i class="fa fa-fw fa-bookmark-o"></i>&nbsp; Profil APIP
                    </button>
                    <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Sejarah</a></li>
                        <li><a href="#">Wilayah</a></li>
                    </ul>
                </li>
                <li class="btn-group">
                    <button type="button" class="btn btn-default btn-sm">
                        <i class="fa fa-fw fa-institution"></i>&nbsp;Pengawasan Daerah
                    </button>
                    <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown">
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Visi dan Misi</a></li>
                        <li><a href="#">Aparatur</a></li>
                    </ul>
                </li>
                <li class="btn-group">
                    <button type="button" class="btn btn-default btn-sm">
                        <i class="fa fa-fw fa-bar-chart-o"></i>&nbsp; Data
                    </button>
                    <button type="button" class="btn btn-warning btn-sm dropdown-toggle" data-toggle="dropdown">
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Data Kapabilitas APIP</a></li>
                        <li><a href="#">Data SDM</a></li>
                        <li><a href="#">Data Keuangan</a></li>
                        <li><a href="#">Data Peraturan</a></li>
                        <li><a href="#">Data Temuan</a></li>
                    </ul>
                </li>
                <li class="btn-group">   
                    <a href="{{Route('login')}}" class="btn btn-sm btn-default"><i class="fa fa-key"></i></a>
                </li>
            @else
                <li class="btn-group">
                    <button type="button" class="btn btn-default btn-sm">
                        <i class="fa fa-fw fa-bookmark-o"></i>&nbsp; Profil APIP
                    </button>
                    <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Sejarah</a></li>
                        <li><a href="#">Wilayah</a></li>
                    </ul>
                </li>
                <li class="btn-group">
                    <button type="button" class="btn btn-default btn-sm">
                        <i class="fa fa-fw fa-institution"></i>&nbsp;Pemerintahan Daerah
                    </button>
                    <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown">
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Visi dan Misi</a></li>
                            <li><a href="#">Aparatur</a></li>
                    </ul>
                </li>
                <li class="btn-group">
                    <button type="button" class="btn btn-default btn-sm">
                        <i class="fa fa-fw fa-bar-chart-o"></i>&nbsp; Data 
                    </button>
                    <button type="button" class="btn btn-warning btn-sm dropdown-toggle" data-toggle="dropdown">
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Data Kapabilitas APIP</a></li>
                            <li><a href="#">Data SDM</a></li>
                            <li><a href="#">Data Keuangan</a></li>
                            <li><a href="#">Data Peraturan</a></li>
                            <li><a href="#">Data Temuan</a></li>
                    </ul>
                </li>
                <li class="btn-group">   
                        <button type="button" class="btn btn-default btn-sm">
                            <i class="fa fa-fw fa-id-badge"></i>&nbsp;  {{ Auth::user()->name }}
                        </button>
                        <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{route('dashboard')}}">Dashboard</a></li>
                            <li><a href="{{url('logout')}}">Logout</a></li>
                        </ul>    
                </li>


                
            @endif
        </ul>
    </div>
</nav>

<style>

.center{
    display: flex;
    align-items: center;
    justify-content: center;
    text-align: center;
    }

</style>


