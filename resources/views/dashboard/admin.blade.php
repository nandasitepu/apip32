@if (Auth::user()->hasRole('admin'))
<div class="panel-body">
    <div class="btn-group">
        <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
          <i class="fa fa-users fa-2x" aria-hidden="true"></i>
        </button>
      <a href="{{route('user.index')}}">
     
          <button type="button" class="btn btn-default btn-sm">
              Users &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
          </button>
        </a>
    </div> 
</div>
<div class="panel-body">
    <div class="btn-group">
        <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-rocket fa-2x" aria-hidden="true"></i>
        </button>
        <button type="button" class="btn btn-default btn-sm">
            Roles &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
        </button> 
    </div> 
</div>  
<div class="panel-body">
    <div class="btn-group">
        <button type="button" class="btn btn-danger btn-sm dropdown-toggle" data-toggle="dropdown">
          <i class="fa fa-random fa-2x" aria-hidden="true"></i>
        </button>
        <button type="button" class="btn btn-default btn-sm">
            Permission &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; 
        </button> 
    </div> 
</div>
@endif
 


