@extends('_l.main') 
@section('title') 
    MEDIA KOMUNIKASI (MAKSI) - APIP SULAWESI BARAT - DASHBOARD

@endsection
@section('extra')
<style>
    body {
        background: url('/img/desa1.jpg') no-repeat center center fixed;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        background-size: cover;
        -o-background-size: cover;
    }
    #transparansi {
        background: url('/img/pages/data1.jpg') no-repeat center center fixed;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        background-size: cover;
        -o-background-size: cover;
    }

    .btn-group.special {
      display: flex;
    }

    .special .btn {
      flex: 2
    }
</style>

@endsection

@section('content')
<div class="row">
        <div class="col-md-3">       
            <div class="panel panel-default">
              <div class="panel-body">
                  <div class="btn-group">
                      <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-building-o fa-2x" aria-hidden="true"></i>
                      </button>
                      <button type="button" class="btn btn-default btn-sm">
                          APIP &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;  &emsp; &emsp; &emsp; 
                      </button> 
                  </div> 
              </div>
              <div class="panel-body">
                  <div class="btn-group">
                      <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-commenting-o fa-2x" aria-hidden="true"></i>
                      </button>
                      <button type="button" class="btn btn-default btn-sm">
                          Forum &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;  &emsp; &emsp; 
                      </button> 
                  </div> 
              </div>
              <div class="panel-body">
                  <div class="btn-group">
                      <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-calendar-check-o fa-2x" aria-hidden="true"></i>
                      </button>
                      <button type="button" class="btn btn-default btn-sm">
                          Event &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;  &emsp; &emsp; 
                      </button> 
                  </div> 
              </div>
              <div class="panel-body">
                  <div class="btn-group">
                      <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-newspaper-o fa-2x" aria-hidden="true"></i>
                      </button>
                      <button type="button" class="btn btn-default btn-sm">
                          Berita &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;  &emsp; &emsp; 
                      </button> 
                  </div> 
              </div>
              <div class="panel-body">
                  <div class="btn-group">
                      <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-users fa-2x" aria-hidden="true"></i>
                      </button>
                      <button type="button" class="btn btn-default btn-sm">
                          Users &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
                      </button>
                  </div> 
              </div>
              <div class="panel-body">
                  <div class="btn-group">
                      <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown">
                          <i class="fa fa-rocket fa-2x" aria-hidden="true"></i>
                      </button>
                      <button type="button" class="btn btn-default btn-sm">
                          Roles &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
                      </button> 
                  </div> 
              </div>  
              <div class="panel-body">
                  <div class="btn-group">
                      <button type="button" class="btn btn-danger btn-sm dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-random fa-2x" aria-hidden="true"></i>
                      </button>
                      <button type="button" class="btn btn-default btn-sm">
                          Permission &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; 
                      </button> 
                  </div> 
              </div>
            </div>
        </div>    
        <div class="col-md-9">
          
              <div class="panel panel-default">
                <div class="panel-body">
                  <div class="hidden-xs">
                    {{-- Home Button --}}
                    <div class="col-md-1"> 
                    <a href="{{route('welcome')}}"> <span class="btn btn-default btn-xs"><i class="fa fa-home fa-fw fa-2x"></i></span></a>
                    </div>
                    
                    {{-- Notifikasi --}}
                    <div class="col-md-7 text-center">
                      <span>
                        <i class="fa fa-bell-o fa-fw"></i>&nbsp; <small>Notifikasi</small> &nbsp;<span class="label label-primary">Go</span> &nbsp;
                        <i class="fa fa-comment-o fa-fw"></i>&nbsp; <small>Komentar</small> &nbsp;<span class="label label-info">Go</span> &nbsp;
                        <i class="fa fa-envelope-o fa-fw"></i>&nbsp; <small>Kontak</small> &nbsp;<span class="label label-danger">Go</span> &nbsp;
                      </span>
                    </div>
                
                    {{-- User Button --}}
                    <div class="col-md-4 text-right">
                      <div class="btn-group">
                        <button type="button" class="btn btn-default btn-sm">
                            <i class="fa fa-user-circle-o" aria-hidden="true"></i> &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;  &emsp; &emsp; 
                        </button> 
                        <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-caret-down" aria-hidden="true"></i>
                        </button>
                       
                        <ul class="dropdown-menu">
                            <li><a href="#"><i class="fa fa-edit fa-fw"></i><small> Profil</small></a></li>
                        </ul>
                        {{-- <a class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" href="#">
                          <i class="fa fa-user-circle-o fa-fw"></i> <span class="fa fa-caret-down" title="Toggle dropdown menu"></span>
                        </a>
                         --}}
                      </div>
                    </div>
                  </div>
                  <div class="visible-xs">
                    <div class="col-xs-12">
                      <i class="text-center"><span class="label label-info">Selamat Datang</span></i>
                      <br>
                    </div>
                    <div class="col-xs-12">
                      <i class="fa fa-bell-o fa-fw"></i>Message &nbsp;<span class="label label-primary">Go</span>
                    </div>
                    <div class="col-xs-12">
                      <i class="fa fa-comment-o fa-fw"></i>Comments &nbsp;<span class="label label-success">Go</span>
                    </div>
                    <div class="col-xs-12">
                      <i class="fa fa-envelope-o fa-fw"></i>Email &nbsp;<span class="label label-info">32</span>
                    </div>
                  </div>
                </div>
              </div>
   
         
       
           
            <div class="panel panel-default">
                <div class="panel-body">
                    <div> <h4>Manage Users Roles</h4></div>
                    <hr>
                    <div class="table-responsive">
                      <table class="table table-bordered table-condensed">
                          <thead>
                              <th class="text-center"><u>Nama</u></th>
                              <th class="text-center"><u>e-Mail</u></th>
                              <th class="text-center">Role</th>
                              <th class="text-center btn btn-success btn-block">Btn</th>       
                          </thead>
                          <tbody>
                              @foreach($users as $user)
                                
                                      <form action="{{ route('user.role') }}" method="POST">
                                        <input name="_method" type="hidden" value="PUT">
                      
                                        <tr>
                                          <td><b>{{ $user->name }}</b></td>
                                          <td>{{ $user->email }} <input type="hidden" name="email" value="{{ $user->email }}"></td>
                                   
                                          <td class="text-center">
                                            <i class="btn btn-default btn-xs fa fa-check-square-o" data-toggle="collapse" data-target="#role{{$user->id}}" aria-hidden="true"></i>
                                          </td> 
                                          <td class="text-center"><button class="btn btn-default btn-sm" type="submit">Update</button></td>
            
                                        
                                        </tr>
                                        <tr >
                                            <td colspan="6" class="hiddenRow well">
                                              <div id="role{{$user->id}}" class="collapse text-center">
                                                 <label class="checkbox-inline"><input type="checkbox"  {{ $user->hasRole('admin') ? 'checked' : '' }} name="role_admin"> Admin</label>
                                                 <label class="checkbox-inline"><input type="checkbox"  {{ $user->hasRole('user') ? 'checked' : '' }} name="role_user"> User</label>
                                                 <label class="checkbox-inline"><input type="checkbox" {{ $user->hasRole('apip') ? 'checked' : '' }} name="role_apip"> APIP</label>
                                                 <label class="checkbox-inline"><input type="checkbox" {{ $user->hasRole('bpkp_pusat') ? 'checked' : '' }} name="role_bpkp_pusat"> BPKP Pusat</label>
                                                 <label class="checkbox-inline"><input type="checkbox" {{ $user->hasRole('bpkp_sulbar') ? 'checked' : '' }} name="role_bpkp_sulbar"> BPKP Sulawesi Barat</label>
                                              </div>
                                              
                                            </td>
                                        </tr>
                                        {{ csrf_field() }}
                                      </form>
                                  
                              @endforeach
                         
                          </tbody>
                      </table>
                   </div>
                </div>
            </div>
                    
                  
                  
           
     
          
        </div>
        {{-- Vertical Menu --}}
        
       
</div>
@endsection



@section('content')
  
@endsection