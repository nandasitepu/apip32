@foreach ($elemen6 as $e6)
<tr class="text-center">
    <td class="col-md-1">
        {{$e6->apip_kapabilitas_id}}
    </td>
    <td class="col-md-4 text-justify">
        {{$e6->apip_kapabilitas->pernyataan}}
    </td>
    <td class="col-md-1">
        {{$e6->apip_kapabilitas->elemen_id}}
    </td>
    <td class="col-md-1">
        {{$e6->apip_level_id}}
    </td>
    <td class="col-md-1">
        {{$e6->apip_kapabilitas->no_pernyataan}}
    </td>
    <td class="col-md-1">
        {{$e6->status}}
    </td>
    <td class="col-md-1">
        <a href="{{$e6->status}}"><i class="fa fa-upload"></i></a>
    </td>
 
    <td class="col-md-1">
        {{$e6->catatan}}
    </td>
</tr>
@endforeach
