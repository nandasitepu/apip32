

@foreach ($elemen2 as $e2)
<tr class="text-center">
    <td class="col-md-1">
        {{$e2->apip_kapabilitas_id}}
    </td>
    <td class="col-md-4 text-justify">
        {{$e2->apip_kapabilitas->pernyataan}}
    </td>
    <td class="col-md-1">
        {{$e2->apip_kapabilitas->elemen_id}}
    </td>
    <td class="col-md-1">
        {{$e2->apip_level_id}}
    </td>
    <td class="col-md-1">
        {{$e2->apip_kapabilitas->no_pernyataan}}
    </td>
    <td class="col-md-1">
        {{$e2->status}}
    </td>
    <td class="col-md-1">
        <i class="fa fa-upload"></i>
    </td>
 
    <td class="col-md-1">
        {{$e2->catatan}}
    </td>
</tr>
@endforeach
