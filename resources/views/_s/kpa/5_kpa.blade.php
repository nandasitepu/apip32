<div class="btn-group">
        <button type="button" class="btn btn-default"><b>Level 2</b></button>
        <button type="button" class="btn btn-default"><b>Elemen 3</b></button>
<button type="button" class="btn btn-default"><b>KPA 2.3.2 - Praktik Profesional</b></button>
    </div>
    <hr>
    @foreach ($kpa5 as $k5)
    <tr class="text-center">
        <td>
            {{$k5->no}}
        </td>
        <td class="text-left">
            <button class="btn btn-default btn-sm" data-toggle="collapse" data-target="#kpa1{{$k5->id}}">
              {{$k5->implementasi}} 
            </button>
        </td>
        <td class="col-md-2 text-center" >
            {{-- Dokumen --}}
            @if ($k5->dokumen ==! null or $k5->dokumen ==! 0)
                <i class="fa fa-file-pdf-o" aria-hidden="true"></i> 
            @else
               <i class="fa fa-folder-open-o" aria-hidden="true"></i> Belum ada
            @endif
           
        </td>
        <td class="text-center">
            @if ($k5->status ==! null or $k5->status ==! 0)
              <i class="fa fa-lg fa-check-circle-o text-success" aria-hidden="true"></i>
            @else
              <i class="fa fa-lg fa-times-circle-o text-danger" aria-hidden="true"></i>
            @endif
      
        </td>
    </tr>
    <tr>
        <td colspan="6" class="hiddenRow">
            <div id="kpa1{{ $k5->id }}" class="collapse">
            
              @if ($k5->keterangan == null)
                Ket:&nbsp;<h5 style="padding-left:40px">Belum ada implementasi</h5>
              @else
                Ket:&nbsp;<h5 style="padding-left:40px">{{$k5->keterangan}}</h5>
              @endif
    
            </div>
        </td>
      </tr>
    @endforeach
    
    