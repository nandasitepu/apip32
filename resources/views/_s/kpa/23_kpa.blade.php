<div class="btn-group">
        <button type="button" class="btn btn-success"><b>Level 3</b></button>
        <button type="button" class="btn btn-default"><b>Elemen 6</b></button>
        <button type="button" class="btn btn-default"><b>KPA 3.6.1 - Mekanisme Penganggaran</b></button>
    </div>
    <hr>
    @foreach ($kpa23 as $k23)
    <tr class="text-center">
        <td>
            {{$k23->no}}
        </td>
        <td class="text-left">
            <button class="btn btn-default btn-sm" data-toggle="collapse" data-target="#kpa1{{$k23->id}}">
                  {{$k23->implementasi}} 
            </button>
        </td>
        <td class="col-md-2 text-center" >
            {{-- Dokumen --}}
            @if ($k23->dokumen ==! null or $k23->dokumen ==! 0)
                <i class="fa fa-file-pdf-o" aria-hidden="true"></i> 
            @else
               <i class="fa fa-folder-open-o" aria-hidden="true"></i> Belum ada
            @endif
           
        </td>
        <td class="text-center">
            @if ($k23->status ==! null or $k23->status ==! 0)
              <i class="fa fa-lg fa-check-circle-o text-success" aria-hidden="true"></i>
            @else
              <i class="fa fa-lg fa-times-circle-o text-danger" aria-hidden="true"></i>
            @endif
      
        </td>
    </tr>
    <tr>
        <td colspan="6" class="hiddenRow">
            <div id="kpa1{{ $k23->id }}" class="collapse">
            
              @if ($k23->keterangan == null)
                Ket:&nbsp;<h5 style="padding-left:40px">Belum ada implementasi</h5>
              @else
                Ket:&nbsp;<h5 style="padding-left:40px">{{$k23->keterangan}}</h5>
              @endif
    
            </div>
        </td>
      </tr>
    @endforeach
    
    