<div class="btn-group">
    <button type="button" class="btn btn-default"><b>Level 2</b></button>
    <button type="button" class="btn btn-default"><b>Elemen 1</b></button>
    <button type="button" class="btn btn-default"><b>KPA 2.1.1 - Audit Ketaatan</b></button>
</div>
<hr>
@foreach ($kpa1 as $k1)
<tr class="text-center">
    <td>
        {{$k1->no}}
    </td>
    <td class="text-left">
        <button class="btn btn-default btn-sm" data-toggle="collapse" data-target="#kpa1{{$k1->id}}">
            {{ $k1->implementasi }} 
        </button>
    </td>
    <td class="col-md-2 text-center" >
        {{-- Dokumen --}}
        @if ($k1->dokumen ==! null or $k1->dokumen ==! 0)
            <a href="{{$k1->dokumen}}" target="_blank"> <i class="fa fa-file-pdf-o" aria-hidden="true"></i> </a>
        @else
           <i class="fa fa-folder-open-o" aria-hidden="true"></i> Belum ada
        @endif
       
    </td>
    <td class="text-center">
        @if ($k1->status ==! null or $k1->status ==! 0)
          <i class="fa fa-lg fa-check-circle-o text-success" aria-hidden="true"></i>
        @else
          <i class="fa fa-lg fa-times-circle-o text-danger" aria-hidden="true"></i>
        @endif
    </td>
</tr>

<div colspan="6" class="hiddenRow">
    <div id="kpa1{{ $k1->id }}" class="collapse"> 
      @if ($k1->keterangan == null)
        Ket:&nbsp;<h5 style="padding-left:40px">Belum ada implementasi</h5>
      @else
        Ket:&nbsp;<h5 style="padding-left:40px">{{ $k1->keterangan }}</h5>
      @endif
    </div>
</div>

@endforeach

