<div class="btn-group">
        <button type="button" class="btn btn-success"><b>Level 3</b></button>
        <button type="button" class="btn btn-default"><b>Elemen 4</b></button>
        <button type="button" class="btn btn-default"><b>KPA 3.4.1 - Pelaporan Manajemen</b></button>
    </div>
    <hr>
    @foreach ($kpa18 as $k18)
    <tr class="text-center">
        <td>
            {{$k18->no}}
        </td>
        <td class="text-left">
            <button class="btn btn-default btn-sm" data-toggle="collapse" data-target="#kpa1{{$k18->id}}">
                  {{$k18->implementasi}} 
            </button>
        </td>
        <td class="col-md-2 text-center" >
            {{-- Dokumen --}}
            @if ($k18->dokumen ==! null or $k18->dokumen ==! 0)
                <i class="fa fa-file-pdf-o" aria-hidden="true"></i> 
            @else
               <i class="fa fa-folder-open-o" aria-hidden="true"></i> Belum ada
            @endif
           
        </td>
        <td class="text-center">
            @if ($k18->status ==! null or $k18->status ==! 0)
              <i class="fa fa-lg fa-check-circle-o text-success" aria-hidden="true"></i>
            @else
              <i class="fa fa-lg fa-times-circle-o text-danger" aria-hidden="true"></i>
            @endif
      
        </td>
    </tr>
    <tr>
        <td colspan="6" class="hiddenRow">
            <div id="kpa1{{ $k18->id }}" class="collapse">
            
              @if ($k18->keterangan == null)
                Ket:&nbsp;<h5 style="padding-left:40px">Belum ada implementasi</h5>
              @else
                Ket:&nbsp;<h5 style="padding-left:40px">{{$k18->keterangan}}</h5>
              @endif
    
            </div>
        </td>
      </tr>
    @endforeach
    
    