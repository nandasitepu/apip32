<div class="btn-group">
        <button type="button" class="btn btn-default"><b>Level 3</b></button>
        <button type="button" class="btn btn-default"><b>Elemen 1</b></button>
        <button type="button" class="btn btn-default"><b>KPA 3.1.2 - Jasa Konsultansi</b></button>
    </div>
    <hr>
    @foreach ($kpa12 as $k12)
    <tr class="text-center">
        <td>
            {{$k12->no}}
        </td>
        <td class="text-left">
            <button class="btn btn-default btn-sm" data-toggle="collapse" data-target="#kpa1{{$k12->id}}">
                  {{$k12->implementasi}} 
            </button>
        </td>
        <td class="col-md-2 text-center" >
            {{-- Dokumen --}}
            @if ($k12->dokumen ==! null or $k12->dokumen ==! 0)
                <i class="fa fa-file-pdf-o" aria-hidden="true"></i> 
            @else
               <i class="fa fa-folder-open-o" aria-hidden="true"></i> Belum ada
            @endif
           
        </td>
        <td class="text-center">
            @if ($k12->status ==! null or $k12->status ==! 0)
              <i class="fa fa-lg fa-check-circle-o text-success" aria-hidden="true"></i>
            @else
              <i class="fa fa-lg fa-times-circle-o text-danger" aria-hidden="true"></i>
            @endif
      
        </td>
    </tr>
    <tr>
        <td colspan="6" class="hiddenRow">
            <div id="kpa1{{ $k12->id }}" class="collapse">
            
              @if ($k12->keterangan == null)
                Ket:&nbsp;<h5 style="padding-left:40px">Belum ada implementasi</h5>
              @else
                Ket:&nbsp;<h5 style="padding-left:40px">{{$k12->keterangan}}</h5>
              @endif
    
            </div>
        </td>
      </tr>
    @endforeach
    
    