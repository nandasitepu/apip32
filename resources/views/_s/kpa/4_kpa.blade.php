<div class="btn-group">
        <button type="button" class="btn btn-default"><b>Level 2</b></button>
        <button type="button" class="btn btn-default"><b>Elemen 3</b></button>
        <button type="button" class="btn btn-default"><b>KPA 2.3.1 - Kerangka Perencanaan Pengawasan</b></button>
    </div>
    <hr>
    @foreach ($kpa4 as $k4)
    <tr class="text-center">
        <td>
            {{$k4->no}}
        </td>
        <td class="text-left">
            <button class="btn btn-default btn-sm" data-toggle="collapse" data-target="#kpa1{{$k4->id}}">
              {{$k4->implementasi}} 
            </button>
        </td>
        <td class="col-md-2 text-center" >
            {{-- Dokumen --}}
            @if ($k4->dokumen ==! null or $k4->dokumen ==! 0)
                <i class="fa fa-file-pdf-o" aria-hidden="true"></i> 
            @else
               <i class="fa fa-folder-open-o" aria-hidden="true"></i> Belum ada
            @endif
           
        </td>
        <td class="text-center">
            @if ($k4->status ==! null or $k4->status ==! 0)
              <i class="fa fa-lg fa-check-circle-o text-success" aria-hidden="true"></i>
            @else
              <i class="fa fa-lg fa-times-circle-o text-danger" aria-hidden="true"></i>
            @endif
      
        </td>
    </tr>
    <tr>
        <td colspan="6" class="hiddenRow">
            <div id="kpa1{{ $k4->id }}" class="collapse">
            
              @if ($k4->keterangan == null)
                Ket:&nbsp;<h5 style="padding-left:40px">Belum ada implementasi</h5>
              @else
                Ket:&nbsp;<h5 style="padding-left:40px">{{$k4->keterangan}}</h5>
              @endif
    
            </div>
        </td>
      </tr>
    @endforeach
    
    