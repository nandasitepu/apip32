<div class="btn-group">
        <button type="button" class="btn btn-default"><b>Level 2</b></button>
        <button type="button" class="btn btn-default"><b>Elemen 4</b></button>
<button type="button" class="btn btn-default"><b>KPA 2.4.1 - Perencanaan Pengawasan</b></button>
    </div>
    <hr>
    @foreach ($kpa6 as $k6)
    <tr class="text-center">
        <td>
            {{$k6->no}}
        </td>
        <td class="text-left">
            <button class="btn btn-default btn-sm" data-toggle="collapse" data-target="#kpa1{{$k6->id}}">
              {{$k6->implementasi}} 
            </button>
        </td>
        <td class="col-md-2 text-center" >
            {{-- Dokumen --}}
            @if ($k6->dokumen ==! null or $k6->dokumen ==! 0)
                <i class="fa fa-file-pdf-o" aria-hidden="true"></i> 
            @else
               <i class="fa fa-folder-open-o" aria-hidden="true"></i> Belum ada
            @endif
           
        </td>
        <td class="text-center">
            @if ($k6->status ==! null or $k6->status ==! 0)
              <i class="fa fa-lg fa-check-circle-o text-success" aria-hidden="true"></i>
            @else
              <i class="fa fa-lg fa-times-circle-o text-danger" aria-hidden="true"></i>
            @endif
      
        </td>
    </tr>
    <tr>
        <td colspan="6" class="hiddenRow">
            <div id="kpa1{{ $k6->id }}" class="collapse">
            
              @if ($k6->keterangan == null)
                Ket:&nbsp;<h5 style="padding-left:40px">Belum ada implementasi</h5>
              @else
                Ket:&nbsp;<h5 style="padding-left:40px">{{$k6->keterangan}}</h5>
              @endif
    
            </div>
        </td>
      </tr>
    @endforeach
    
    