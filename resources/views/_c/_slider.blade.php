<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" id="whole">
        <div id="logo-desa">
            <br>
            <div class="btn btn-default"><i class="fa fa-medium fa-3x" aria-hidden="true"></i></div>
            <br>
            <h2> 
              <span class="label label-success">AKSI</span> 
            </h2>
            <br>
            <a href="/forum"> <button class="btn btn-default">Forum Tanya Jawab &nbsp;<i class="fa fa-search fa-lg"></i></button></a>
            <br>
            <h4><span class="label label-default">Media Komunikasi APIP Sulawesi Barat</span></h4> 
        </div>
        <div class="item active">
        <img src="img/karampuang.jpg" alt="Chania" style="width:100%;height:300px">
            <div class="carousel-caption">
                
            </div>
        </div>

        <div class="item">
        <img src="img/rumah-adat.jpg" alt="Chania" style="width:100%;height:300px">
            <div class="carousel-caption">
            
            </div>
        </div>

        <div class="item">
        <img src="img/mamuju-laut.jpg" alt="Chania" style="width:100%;height:300px">
            <div class="carousel-caption">
                
            </div>
        </div>
    </div>
</div>