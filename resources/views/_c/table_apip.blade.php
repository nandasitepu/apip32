<div class="table-responsive">
  <table class="table table-bordered table-condensed">  
      <tr class="text-center">
        <td class="bold">No</td>
        <td class="bold">Infrastruktur/Dokumen/Pelaksanaan</td>
        <td class="bold">Elemen</td>
        <td class="bold">Level</td>
        <td class="bold">No-PYT</td>
        <td class="bold">Status</td>
        <td class="bold">Upload</td>
        <td class="bold">Catatan</td>
      </tr>
    <tbody>
      {{$slot}}
    </tbody>
  </table>
</div>