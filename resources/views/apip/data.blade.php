@extends('_l.main') 
@section('title') 
    MEDIA KOMUNIKASI (MAKSI) - APIP SULAWESI BARAT

@endsection
@section('extra')
<style>
    body {
        background: url('/img/desa1.jpg') no-repeat center center fixed;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        background-size: cover;
        -o-background-size: cover;
    }
    #transparansi {
        background: url('/img/pages/data1.jpg') no-repeat center center fixed;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        background-size: cover;
        -o-background-size: cover;
    }

    .center-btn {
    display: flex;
    align-items: center;
    justify-content: center;
    }

</style>

@endsection

@section('content')
  <div class="row">
      <div class="col-md-7">
        <div class="text-center bold">   
            @if($apip->id === 1)
            <a href="{{route('apip.data', $apip->id)}}" class="btn disabled">
              <i class="fa fa-lg fa-cube pull-left "></i>
            </a>
            @else
            <a href="{{route('apip.data', $previous)}}">
              <i class="fa fa-lg fa-arrow-circle-o-left pull-left"></i>
            </a>
            @endif
          <b> {{ucwords($apip->nama)}}</b>
          <a href="{{route('apip.data', $next)}}">
            <i class="fa fa-lg fa-arrow-circle-o-right pull-right"></i></a>
        </div>
  
        <div class="panel panel-default">
            <div class="panel-body">
              
                <div class="col-md-3">
                 
    
                  <div class="text-center"><img class="" src="{{asset('img/apip/'. $apip->foto)}}" alt="" height="150px"></div>
                  <br>
                </div>
                <div class="col-md-9">
                  <div class="col-md-8">
                  
                    <p><b><i class="fa fa-star-o fa-fw"></i> Inspektur <i class="fa fa-star-o fa-fw"></i>&nbsp;:</b> 
                    <br>{{ucwords($apip->pimpinan)}} 
                   
                    </p>
                    
                    <hr>
    
                    <p><b>Alamat</b> <i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp; : <br>{!! $apip->kontak !!}</p>
                    <p><b>Kontak</b>&nbsp;:<br>
                      <i class="fa fa-whatsapp fa-lg" aria-hidden="true"></i> <br>
                      <i class="fa fa-envelope-open-o fa-lg" aria-hidden="true"></i>
                    </p>
                    <p><b>Info</b> <i class="fa fa-lightbulb-o fa-fw"></i>&nbsp;: <br>{{$apip->keterangan}}</p>
                    
                  </div>
                  <div class="col-md-4">
                    <div class="center">
                          <img class="" src="{{asset('img/kab/'. $apip->gambar)}}" height="50px">
                          
                    </div><br><a href="{{route('apip.detail', $apip->id)}}"><div class="btn btn-default btn-xs btn-block"><b>Detail</b></div></a>
                   
        
                    <p><b>Pegawai<i class="fa fa-users fa-fw text-danger"></i> : </b> <br>{{$apip->total_pegawai}} Orang</p>
                    
                    <p><b>Anggaran<i class="fa fa-money fa-fw text-success"></i>:</b> <br>Rp {{number_format($apip->total_anggaran)}}</p>
                    <p><b>Auditor<i class="fa fa-user-secret" aria-hidden="true"></i> = </b>&nbsp;10 Orang</p>
                    <p><b>P2UPD<i class="fa fa-user-circle-o" aria-hidden="true"></i> = </b>&nbsp;10 Orang</p>
                    
                  </div>
                </div>
            
            </div>
        </div>
        <div class="tab-content center">
            <div class="btn-group">
                <a class="btn btn-default btn-sm boxed" data-toggle="tab" href="#all"><b>Elemen #</b></a>                  
                <a class="btn btn-default btn-sm boxed" data-toggle="tab" href="#elemen1">1</a>              
                <a class="btn btn-primary btn-sm boxed" data-toggle="tab" href="#elemen2">2</a>
                <a class="btn btn-success btn-sm boxed" data-toggle="tab" href="#elemen3">3</a>
                <a class="btn btn-info btn-sm boxed"    data-toggle="tab" href="#elemen4">4</a>     
                <a class="btn btn-warning btn-sm boxed" data-toggle="tab" href="#elemen5">5</a>
                <a class="btn btn-danger btn-sm boxed"  data-toggle="tab" href="#elemen6">6</a>
            </div>
        
        </div>
      </div>
      <div class="col-md-5">
          <br>
              <div class="btn-group-justified">
                  <div class="btn btn-default btn-xs" data-toggle="tab" href="#photo">
                     <i class="fa fa-picture-o" aria-hidden="true"></i>
                  </div>
                  <div class="btn btn-default btn-xs" data-toggle="tab" href="#jasa">
                      <i class="fa fa-building-o" aria-hidden="true"></i>&nbsp; Jasa
                  </div>
                  <div class="btn btn-default btn-xs" data-toggle="tab" href="#elemen">Elemen 
                    <i class="fa fa-star-o"></i> 
                  </div>
                  <div class="btn btn-default btn-xs" data-toggle="tab" href="#pegawai">
                    <i class="fa fa-users" aria-hidden="true"></i>&nbsp; Pegawai
                  </div>
              </div>

              <div class="tab-content">
                  <div id="jasa" class="tab-pane fade in center active">
                    <div> @include('apip.charts.jasa')</div>
                  </div>
                  <div id="elemen" class="tab-pane fade in ">
                      <div> @include('apip.charts.kapabilitas')</div>
                  </div>
                  <div id="pegawai" class="tab-pane fade in center">
                      <div> @include('apip.charts.pegawai')</div>
                  </div>
                  
                  </div>
      </div>
      
  </div>
 
  <div class="row">
    
        <div class="panel panel-default">
          <div id="show_kapabilitas" class="panel-collapse collapse in">
            <div class="panel-body">
            
              <div class="tab-content">
                <div id="all" class="tab-pane fade in active">
                  @component('_c._table_all_apip')
                    @include('_s.kpa.0_all_kpa')
                  @endcomponent
                </div>
                <div id="elemen1" class="tab-pane fade in">
                    {{-- L2 -  Elemen 1 - KPA 2.1.1 - Audit Ketaatan --}}
                    @component('_c._table_kpa_apip')
                    @include('_s.kpa.1_kpa')
                    @endcomponent

                    {{-- LEVEL 3 --}}

                    {{-- L3 -  Elemen 1 - KPA 3.1.1 - Audit Kinerja --}}
                    @component('_c._table_kpa_apip')
                    @include('_s.kpa.11_kpa')
                    @endcomponent
                    {{-- L3 -  Elemen 1 - KPA 3.1.2 - Jasa Konsultansi --}}
                    @component('_c._table_kpa_apip')
                    @include('_s.kpa.12_kpa')
                    @endcomponent
                  
                </div>
                <div id="elemen2" class="tab-pane fade in">
                      {{-- L2 - Elemen 2 - KPA 2.2.1 - Identifikasi SDM--}}
                      @component('_c._table_kpa_apip')
                      @include('_s.kpa.2_kpa') 
                      @endcomponent
  
                      {{-- L2 - Elemen 2 - KPA 2.2.2 - Pengembangan Profesi--}}
                      @component('_c._table_kpa_apip')
                      @include('_s.kpa.3_kpa')
                      @endcomponent
                      
                      {{-- LEVEL 3 --}}

                      {{-- L3 - Elemen 2 - KPA 3.2.1 - Koordinasi SDM APIP--}}
                      @component('_c._table_kpa_apip')
                      @include('_s.kpa.13_kpa')
                      @endcomponent

                      {{-- L3 - Elemen 2 - KPA 3.2.2 - Staf APIP Berkualifikasi Profesional--}}
                      @component('_c._table_kpa_apip')
                      @include('_s.kpa.14_kpa')
                      @endcomponent

                      {{-- L3 - Elemen 2 - KPA 3.2.3 - Kompetensi dan Team Building--}}
                      @component('_c._table_kpa_apip')
                      @include('_s.kpa.15_kpa')
                      @endcomponent
                </div>

                <div id="elemen3" class="tab-pane fade in">
                    {{-- L2 - Elemen 3 - KPA 2.3.1 - Kerangka Perencanaan Pengawasan--}}
                    @component('_c._table_kpa_apip')
                    @include('_s.kpa.4_kpa') 
                    @endcomponent

                    {{-- L2 - Elemen 3 - KPA 2.3.2 - Praktik Profesional--}}
                    @component('_c._table_kpa_apip')
                    @include('_s.kpa.5_kpa')
                    @endcomponent

                    {{-- LEVEL 3 --}}
                    {{-- L3 - Elemen 3 - KPA 3.3.1 - Perencanaan Basis Risiko--}}
                    @component('_c._table_kpa_apip')
                    @include('_s.kpa.16_kpa') 
                    @endcomponent

                      {{-- L3 - Elemen 3 - KPA 3.3.2 - Kerangka Kerja Mengelola Kualitas--}}
                    @component('_c._table_kpa_apip')
                    @include('_s.kpa.17_kpa') 
                    @endcomponent
                </div>

                <div id="elemen4" class="tab-pane fade in">
                    {{-- L2 - Elemen 4 - KPA 2.4.1 - Perencanaan Pengawasan--}}
                    @component('_c._table_kpa_apip')
                    @include('_s.kpa.6_kpa') 
                    @endcomponent

                    {{-- L2 - Elemen 4 - KPA 2.4.2 - Anggaran Operasional--}}
                    @component('_c._table_kpa_apip')
                    @include('_s.kpa.7_kpa')
                    @endcomponent
                    
                    {{-- LEVEL 3 --}}
                    
                    {{-- L3 - Elemen 4 - KPA 3.4.1 - Pelaporan Manajemen--}}
                    @component('_c._table_kpa_apip')
                    @include('_s.kpa.18_kpa')
                    @endcomponent

                    {{-- L3 - Elemen 4 - KPA 3.4.2 - Informasi Biaya--}}
                    @component('_c._table_kpa_apip')
                    @include('_s.kpa.19_kpa')
                    @endcomponent

                    {{-- L3 - Elemen 4 - KPA 3.4.3 - Pengukuran Kinerja--}}
                    @component('_c._table_kpa_apip')
                    @include('_s.kpa.20_kpa')
                    @endcomponent
                </div>

                <div id="elemen5" class="tab-pane fade in">
                    {{-- L2 - Elemen 5 - KPA 2.5.1 - Pengelolaan Bisnis Proses Pengawasan Intern--}}
                    @component('_c._table_kpa_apip')
                    @include('_s.kpa.8_kpa') 
                    @endcomponent

                    {{-- LEVEL 3 --}}

                    {{-- L3 - Elemen 5 - KPA 3.5.1 - Komponen Tim Manajemen--}}
                    @component('_c._table_kpa_apip')
                    @include('_s.kpa.21_kpa')
                    @endcomponent

                    {{-- L3 - Elemen 5 - KPA 3.5.2 - Koordinasi Pihak Lain--}}
                    @component('_c._table_kpa_apip')
                    @include('_s.kpa.22_kpa')
                    @endcomponent
                </div>

                <div id="elemen6" class="tab-pane fade in">
                    {{-- L2 - Elemen 6 - KPA 2.6.1 - Hubungan Pelaporan--}}
                    @component('_c._table_kpa_apip')
                    @include('_s.kpa.9_kpa') 
                    @endcomponent

                    {{-- L2 - Elemen 6 - KPA 2.6.2 - Akses Penuh Sumber Daya--}}
                    @component('_c._table_kpa_apip')
                    @include('_s.kpa.10_kpa')
                    @endcomponent

                    {{-- LEVEL 3 --}}

                    {{-- L3 - Elemen 6 - KPA 3.6.1 - Mekanisme Penganggaran--}}
                    @component('_c._table_kpa_apip')
                    @include('_s.kpa.23_kpa')
                    @endcomponent

                    {{-- L3 - Elemen 6 - KPA 3.6.2 - Pengawasan Manajemen--}}
                    @component('_c._table_kpa_apip')
                    @include('_s.kpa.24_kpa')
                    @endcomponent
                </div>
                

               
              </div>
            </div>
          </div>
        </div>
    
    {{-- ACCORDION DATA End --}}
  </div> 
@endsection
