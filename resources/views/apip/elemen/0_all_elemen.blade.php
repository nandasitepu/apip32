

@foreach ($all as $e)
<tr class="text-center">
    <td class="col-md-1">
        {{$e->apip_kapabilitas_id}}
    </td>
    <td class="col-md-4 text-justify">
        {{$e->apip_kapabilitas->pernyataan}}
    </td>
    <td class="col-md-1 hiddenCol">
        {{$e->apip_kapabilitas->elemen_id}}
    </td>
    <td class="col-md-1">
        {{$e->apip_level_id}}
    </td>
    <td class="col-md-1">
        {{$e->apip_kapabilitas->no_pernyataan}}
    </td>
    <td class="col-md-1">
      @if ($e->status == 1)
        <i class="fa fa-check-circle-o fa-2x text-success"></i>
      @elseif ($e->status == 2 )
        <i class="fa fa-times-circle-o fa-2x text-danger"></i>
      @else
        <i class="fa fa-question-circle fa-2x text-warning"></i>
      @endif
    </td>
    <td class="col-md-1">
        <a href="{{$e->apip_data->upload}}" target="_blank"><i class="fa fa-upload fa-2x"></i></a>
    </td>
    <td class="col-md-1 text-center" data-toggle="collapse" data-target="#all{{$e->id}}">
        @if($e->catatan)
            <i class="fa fa-2x fa-plus-square-o text-danger"></i>
        @else
            <i class="fa fa-2x fa-minus-square-o text-info"></i>
        @endif
    </td>
</tr>

<tr>
    <td colspan="6" class="hiddenRow">
        <div id="all{{$e->id}}" class="collapse">
          @if ($e->catatan)
            <h5 style="padding-left:40px">{{$e->catatan}}</h5>
          @else
            <h5 style="padding-left:40px">Belum ada Catatan</h5>
          @endif

        </div>
    </td>
</tr>
@endforeach

