@foreach ($elemen5 as $e5)
<tr class="text-center">
    <td class="col-md-1">
        {{$e5->apip_kapabilitas_id}}
    </td>
    <td class="col-md-4 text-justify">
        {{$e5->apip_kapabilitas->pernyataan}}
    </td>
    <td class="col-md-1">
        {{$e5->apip_kapabilitas->elemen_id}}
    </td>
    <td class="col-md-1">
        {{$e5->apip_level_id}}
    </td>
    <td class="col-md-1">
        {{$e5->apip_kapabilitas->no_pernyataan}}
    </td>
    <td class="col-md-1">
        {{$e5->status}}
    </td>
    <td class="col-md-1">
        <i class="fa fa-upload"></i>
    </td>
 
    <td class="col-md-1">
        {{$e5->catatan}}
    </td>
</tr>
@endforeach
