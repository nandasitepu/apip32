@extends('_l.main') 
@section('title') 
    MEDIA KOMUNIKASI (MAKSI) - APIP SULAWESI BARAT

@endsection
@section('extra')
<style>
    body {
        background: url('/img/desa1.jpg') no-repeat center center fixed;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        background-size: cover;
        -o-background-size: cover;
    }
    #transparansi {
        background: url('/img/pages/data1.jpg') no-repeat center center fixed;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        background-size: cover;
        -o-background-size: cover;
    }

    .center-btn {
    display: flex;
    align-items: center;
    justify-content: center;
    }

</style>

@endsection

@section('content')
<div class="row">


    <div class="col-md-12">
        <div class="text-center bold">   
            @if($apip->id === 1)
            <a href="{{route('apip.data', $apip->id)}}" class="btn disabled">
            <i class="fa fa-lg fa-cube pull-left "></i>
            </a>
            @else
            <a href="{{route('apip.data', $previous)}}">
            <i class="fa fa-lg fa-arrow-circle-o-left pull-left"></i>
            </a>
            @endif
        <b> Detail {{ucwords($apip->nama)}}</b>
        <a href="{{route('apip.data', $next)}}">
            <i class="fa fa-lg fa-arrow-circle-o-right pull-right"></i></a>
        </div>

        <div class="panel panel-default">
            <div class="panel-body">
            
                <div class="col-md-3">
                <h5 class="center"><span class="label label-primary">{{ucwords($apip->nama)}}</span></h4 >
    
                <div class="text-center"><img class="img-thumbnail" src="{{asset('img/apip/'. $apip->gambar)}}" alt="" height="10px"></div>
                <br>
                </div>
                <div class="col-md-9">
                <div class="col-md-9">
                
                    <p><b><i class="fa fa-star-o fa-fw"></i> Inspektur <i class="fa fa-star-o fa-fw"></i>&nbsp;:</b> 
                    <br>{{ucwords($apip->pimpinan)}} <span><img class="cool pull-right cool" src="{{asset('img/kab/'. $apip->foto)}}" height="100px"></span>
                    </p>
                    
                    <hr>
    
                    <p><b>Alamat</b> <i class="fa fa-envelope-o fa-fw"></i>&nbsp; : <br>{!! $apip->kontak !!}</p>
                    <p><b>Info</b> <i class="fa fa-lightbulb-o fa-fw"></i>&nbsp;: <br>{{$apip->keterangan}}</p>
                </div>
                <div class="col-md-3">
                    <div class="btn btn-default btn-sm btn-block"><b>Detail</b></div>
                    <hr>
                    <p><b>Pegawai <i class="fa fa-users fa-fw"></i> :</b> <br>{{$apip->total_pegawai}} Orang</p>
                    <p><b>Auditor <i class="fa fa-user-md fa-fw"></i> :</b> <br>10 Orang</p>
                    <p><b>Anggaran <i class="fa fa-money fa-fw"></i>:</b> <br>Rp {{number_format($apip->total_anggaran)}}</p>
                </div>
                </div>
            
            </div>
        </div>
    </div>
    
</div>

  
@endsection
