<div class="row">
    <div class="col-md-3">
        <div class="center" style="display: flex;
        justify-content: center;
        align-items: center;">
           <div class="btn-group-vertical">
                <p> APIP <br>Provinsi Sulawesi Barat</p>
                
                <div class="btn btn-default"><img class="" src="../img/kab/sulbar.jpg" alt="" height="150px"></div>
                <a href="{{route('apip.data', 1)}}" class="btn btn-default btn-xs btn-block">Detail 1</a>
               
           </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="row">
                <div class="center">
                    <div class="btn-group-vertical">
                        <p>APIP Pasangkayu</p>
                        <div class="btn btn-default">
                            <img class="" src="../img/kab/matra.jpg" alt="" height="50px" >
                        </div>
                        
                        <a href="{{route('apip.data', 2)}}" class="btn btn-default btn-xs btn-block"> Detail 2</a>
                    </div>
                </div>
            <hr>
        </div>
        <div class="row">
                <div class="center">
                    <div class="btn-group-vertical">
                        <p>APIP Majene</p>
                        <div class="btn btn-default">
                            <img class="" src="../img/kab/majene.jpg" alt="" height="50px" >
                        </div>
                        <a href="{{route('apip.data', 5)}}" class="btn btn-default btn-xs btn-block"> Detail 5</a>
                    </div>
                </div>
            <hr>
        </div>
       
      
        
       
    </div>
    
    <div class="col-md-3">
            <div class="row">
                    <div class="center">
                        <div class="btn-group-vertical">
                            <p>APIP Mateng</p>
                            <div class="btn btn-default"><img class="" src="../img/kab/mateng.jpg" alt="" height="50px" ></div>
                            <a href="{{route('apip.data', 3)}}" class="btn btn-default btn-xs btn-block"> Detail 3</a>
                        </div>
                    </div>
                <hr>
            </div>
            <div class="row">
                    <div class="center">
                        <div class="btn-group-vertical">
                               <p> APIP Polman</p>
                                <div class="btn btn-default">
                                  <img class="" src="../img/kab/polman.jpg" alt="" height="50px" >
                                </div>
                            
                                <a href="{{route('apip.data', 6)}}" class="btn btn-default btn-xs btn-block"> Detail 6</a>
                        </div>
                    </div>
                <hr>
            </div>
      

            
    </div>
    <div class="col-md-3">
            <div class="row">
                    <div class="center">
                        <div class="btn-group-vertical">
                            <p> APIP Mamuju</p>
                            <div class="btn btn-default">
                                <img class="" src="../img/kab/mamuju.jpg" alt="" height="50px" >
                            </div>
                            <a href="{{route('apip.data', 4)}}" class="btn btn-default btn-xs"> Detail 4</a>
                        </div>
                    </div>
                <hr>
            </div>
           

            <div class="row">
                    <div class="center">
                        <div class="btn-group-vertical">
                                <p>APIP Mamasa</p>
                                <div class="btn btn-default">
                                <img class="" src="../img/kab/mamasa.jpg" alt="" height="50px" >
                                </div>
                                <a href="{{route('apip.data', 7)}}" class="btn btn-default btn-xs btn-block"> Detail 7</a>
                        </div>
                    </div>
                <hr>
            </div>
            

           
            <br>
          </div>
    </div>
       
           
       