@extends('_l.main') 
@section('title') 
    MEDIA KOMUNIKASI (MAKSI) - APIP SULAWESI BARAT

@endsection

@section('extra')
<style>
    body {
        background: url('img/desa1.jpg') no-repeat center center fixed;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        background-size: cover;
        -o-background-size: cover;
    }
    #transparansi {
        background: url('img/pages/data1.jpg') no-repeat center center fixed;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        background-size: cover;
        -o-background-size: cover;
    }

    .rotate-45-right {
    display: inline-block;
    -webkit-transform: rotate(45deg);
    transform: rotate(45deg);
}
</style>


@endsection

<!-- Chart Script -->


@section('content')
      <div class="row">
          <div class="col-md-7">
              <h4><span>Aparat Pengawasan Intern Pemerintah (APIP) |</span>
              &nbsp; <a href="#" class="label label-info">Peningkatan Kapabilitas <i class="fa fa-fw fa-arrow-up rotate-45-right fa-lg "></i></a> </span>
              </h4>
              <hr>
 
              <div class="main">
              <p>Adalah instansi pemerintah yang dibentuk dengan tugas melaksanakan pengawasan intern (audit intern) di lingkungan pemerintah pusat dan/atau pemerintah daerah,</p>
              <blockquote cite="http://www.bpkp.go.id">
              <small> terdiri dari:
                      <li>Badan Pengawasan Keuangan dan Pembangunan (BPKP)</li>
                      <li>Inspektorat jenderal kementerian, inspektorat/unit pengawasan intern pada kementerian negara, inspektorat utama/inspektorat lembaga pemerintah non kementerian, inspektorat/unit pengawasan intern pada kesekretariatan lembaga tinggi negara dan lembaga negara, </li>
                      <li>Inspektorat provinsi/kabupaten/kota; dan unit pengawasan intern pada badan hukum pemerintah lainnya sesuai dengan peraturan perundang-undangan</li>
  
              </small>
              </blockquote>
              </div>
          </div>
          <div class="col-md-5">@include('charts.apip.sulbar')</div>
          
        
      </div>
      <div class="row">
          <div class="text-center">
            <div class="panel panel-default">
         <div class="panel-body">
                <div class="col-md-1 col-xs-10">
                  APIP Sulbar
                  <div class="">
                    <img class="img-thumbnail" src="../img/kab/sulbar.jpg" alt="">
                    <a href="{{route('apip.data', 1)}}" class="btn btn-default btn-xs btn-block">Detail</a>
                  </div>
                  <br>
                </div>

                <div class="col-md-1 col-xs-6 ">
                  APIP Pasangkayu
                  <div class="">
                    <img class="img-thumbnail" src="../img/kab/matra.jpg" alt="" height="100px" >
                  </div>
               
                  <a href="{{route('apip.data', 2)}}" class="btn btn-default btn-xs btn-block"> Detail</a>
                  <br>
                </div>
                
                <div class="col-md-1 col-xs-6 ">
                  APIP Mateng
                  <div class="">
                    <img class="img-thumbnail" src="../img/kab/mateng.jpg" alt="" height="100px" >
                  </div>
                
                  <a href="{{route('apip.data', 3)}}" class="btn btn-default btn-xs btn-block"> Detail</a>
                  <br>
                </div>

                <div class="col-md-1 col-xs-6 ">
        
                  APIP Mamuju
                  <div class="">
                    <img class="img-thumbnail" src="../img/kab/mamuju.jpg" alt="" height="100px" >
                  </div>
               
                  <a href="{{route('apip.data', 4)}}" class="btn btn-default btn-xs btn-block"> Detail</a>
                  <br>
                </div>
                <div class="col-md-1 col-xs-6 ">
                  APIP Majene
                  <div class="">
                    <img class="img-thumbnail" src="../img/kab/majene.jpg" alt="" height="100px" >
                  </div>
                  <a href="{{route('apip.data', 5)}}" class="btn btn-default btn-xs btn-block"> Detail</a>
                  <br>
                </div>
                <div class="col-md-1 col-xs-6 ">
                  APIP Polman
                  <div class="">
                    <img class="img-thumbnail" src="../img/kab/polman.jpg" alt="" height="100px" >
                  </div>
              
                  <a href="{{route('apip.data', 6)}}" class="btn btn-default btn-xs btn-block"> Detail</a>
                  <br>
                </div>
                <div class="col-md-1 col-xs-6 ">
                  APIP Mamasa
                  <div class="">
                    <img class="img-thumbnail" src="../img/kab/mamasa.jpg" alt="" height="100px" >
                  </div>
                  <a href="{{route('apip.data', 7)}}" class="btn btn-default btn-xs btn-block"> Detail</a>
                  <br>
                </div>
          
         </div>
            </div>
          </div>
    
          
      </div>
@endsection