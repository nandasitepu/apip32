<div>
        <canvas id="chart_pegawai" height="200%"></canvas>
      </div>
      <script src="{{asset('js/jquery/jquery-1.12.4.min.js')}}"></script>
      <script src="{{asset('js/chart/Chart.bundle.js')}}"></script>
      <script src="{{asset('js/chart/Chart.PieceLabel.js')}}"></script>
      
      <!-- Chart Script -->
      <script>
        var ctx = document.getElementById("chart_pegawai").getContext('2d');
        var myChart = new Chart(ctx, {
          type: 'pie',
          data: {
          labels: ['Struktural', 'Auditor', 'P2UPD', 'Staf'],
          datasets: [{
            backgroundColor: [
              "#e74c3c",
              "#3498db",
           
              "#2ecc71",
              "#95a5a6",  
              "#f1c40f",
      
              "#9b59b6",
              
              
            
           
            
          ],
          data: [4,12,8,16]
        },
       ]
        },
        options: {
          title: {
            display: true,
            text: 'Pegawai APIP '
          },
          legend: {
            position: 'right',
          },
          pieceLabel: {
          mode: 'value',
          arcText: false,
          format: function (value) {
            return value;
          }
        }
      
        }
        });
      </script>
      