<div>
  <canvas id="Elemen" height="200%"></canvas>
</div>
<script src="{{asset('js/jquery/jquery-1.12.4.min.js')}}"></script>
<script src="{{asset('js/chart/Chart.bundle.js')}}"></script>
<script src="{{asset('js/chart/Chart.PieceLabel.js')}}"></script>

<!-- Chart Script -->
<script>
  var ctx = document.getElementById("Elemen").getContext('2d');
  var myChart = new Chart(ctx, {
    type: 'doughnut',
    data: {
    labels: ['Assurance', 'Consulting', 'Lainnya'],
    datasets: [{
      backgroundColor: [
        "#e74c3c",
        "#3498db",
     
        "#2ecc71",
        "#95a5a6",  
        "#f1c40f",

        "#9b59b6",
        
        
      
     
      
    ],
    data: [60,20,20]
  },
 ]
  },
  options: {
    title: {
      display: true,
      text: 'Jasa APIP '
    },
    legend: {
      position: 'right',
    },
    pieceLabel: {
    mode: 'value',
    arcText: false,
    format: function (value) {
      return value;
    }
  }

  }
  });
</script>
