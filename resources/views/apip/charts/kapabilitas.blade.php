<canvas class ="panel" id="APIP" height="200%" ></canvas>
<script src="{{asset('js/jquery/jquery-1.12.4.min.js')}}"></script>
<script src="{{asset('js/chart/Chart.bundle.js')}}"></script>
<script src="{{asset('js/chart/Chart.PieceLabel.js')}}"></script>
<script>
 var chartData = {!! $chart !!};   
 // Draw Chart
 var ctx = document.getElementById("APIP").getContext("2d");
    var myChart = new Chart(ctx, {
        type: 'horizontalBar',
        data: {
            labels:  [
                'Peran dan Layanan',
                'Manajemen SDM',
                'Praktik Profesional',
                'Manajemen Kinerja dan Akuntabilitas',
                'Hubungan dan Budaya Organisasi',
                'Struktur Tata Kelola'
            ],
            datasets: [
                {
                label: 'Nilai per Elemen',
                data: Object.values(chartData) ,
                backgroundColor: [
                    "#fff",
                    "#fff",
                    "#fff",
                    "#fff",
                    "#fff",
                    "#fff",
                ],
                borderColor: [
                    "#2ecc71",
                    "#3498db",
                    "#95a5a6",
                    "#9b59b6",
                    "#f1c40f",
                    "#e74c3c",

                ],
                borderWidth: 2
            },
            ]
        },
        options: {
            scales: {
                xAxes: [{
                    ticks: {
                        beginAtZero:true,
                        max: 5,
                    }
                }],
                yAxes: [{
                    ticks: {
                        fontSize: 12,
                        fontFamily: 'Trebuchet MS',
                        fontStyle	: 'bold'
                    }
                }]
            },
            title: {
                display: true,
                text: 'Kapabilitas APIP per Elemen',
            },


        }
    }); 
       
</script>