<div class="btn-group">
    <button type="button" class="btn btn-default"><b>Level 2</b></button>
    <button type="button" class="btn btn-default"><b>Elemen 2</b></button>
    <button type="button" class="btn btn-default"><b>KPA 2 - Identifikasi SDM</b></button>
</div>
<hr>
@foreach ($kpa5 as $k5)
<tr class="text-center">
    <td>
        {{$k5->no}}
    </td>
    <td class="text-left">
        {{$k5->implementasi}}
    </td>
    <td class="text-left">
        {{$k5->bukti}}
    </td>
    <td class="col-md-2 text-center">
        @if ($k5->status == 1)
            <i class="fa fa-check-circle-o fa-2x text-success"></i>
        @elseif ($k5->status == 0 )
        <i class="fa fa-times-circle-o fa-2x text-danger"></i>
        @else
        <i class="fa fa-question-circle fa-2x text-warning"></i>
        @endif
    </td>
</tr>
<tr>
    </td>
</tr>
@endforeach

