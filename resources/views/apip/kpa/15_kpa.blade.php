<div class="btn-group">
    <button type="button" class="btn btn-default"><b>Level 2</b></button>
    <button type="button" class="btn btn-default"><b>Elemen 2</b></button>
    <button type="button" class="btn btn-default"><b>KPA 2 - Identifikasi SDM</b></button>
</div>
<hr>
@foreach ($kpa15 as $k15)
<tr class="text-center">
    <td>
        {{$k15->no}}
    </td>
    <td class="text-left">
        {{$k15->implementasi}}
    </td>
    <td class="text-left">
        {{$k15->bukti}}
    </td>
    <td class="col-md-2 text-center">
        @if ($k15->status == 1)
            <i class="fa fa-check-circle-o fa-2x text-success"></i>
        @elseif ($k15->status == 0 )
        <i class="fa fa-times-circle-o fa-2x text-danger"></i>
        @else
        <i class="fa fa-question-circle fa-2x text-warning"></i>
        @endif
    </td>
</tr>
<tr>
    </td>
</tr>
@endforeach

