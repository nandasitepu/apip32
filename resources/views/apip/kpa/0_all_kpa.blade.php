<div class="btn-group">
    <button type="button" class="btn btn-default"><b>Level 2-3</b></button>
    <button type="button" class="btn btn-default"><b>Elemen 1-6</b></button>
    <button type="button" class="btn btn-default"><b>KPA 1-24</b></button>
</div>
<hr>
@foreach ($all as $a)
<tr class="text-center">
    <td>
        {{$a->apip_level_id}}
    </td>
    <td>
        {{$a->apip_elemen_id}}
    </td>
    <td>
        {{$a->apip_kpa_id}}
    </td>
    <td class="text-left">
        {{$a->implementasi}}
    </td>
    <td class="text-left">
        {{$a->bukti}}
    </td>
    <td class="col-md-2 text-center">
        @if ($a->status == 1)
            <i class="fa fa-check-circle-o fa-2x text-success"></i>
        @elseif ($a->status == 0 )
        <i class="fa fa-times-circle-o fa-2x text-danger"></i>
        @else
        <i class="fa fa-question-circle fa-2x text-warning"></i>
        @endif
    </td>
</tr>
<tr>
    </td>
</tr>
@endforeach

