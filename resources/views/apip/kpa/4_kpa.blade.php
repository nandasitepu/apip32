<div class="btn-group">
    <button type="button" class="btn btn-default"><b>Level 2</b></button>
    <button type="button" class="btn btn-default"><b>Elemen 2</b></button>
    <button type="button" class="btn btn-default"><b>KPA 2 - Identifikasi SDM</b></button>
</div>
<hr>
@foreach ($kpa4 as $k4)
<tr class="text-center">
    <td>
        {{$k4->no}}
    </td>
    <td class="text-left">
        {{$k4->implementasi}}
    </td>
    <td class="text-left">
        {{$k4->bukti}}
    </td>
    <td class="col-md-2 text-center">
        @if ($k4->status == 1)
            <i class="fa fa-check-circle-o fa-2x text-success"></i>
        @elseif ($k4->status == 0 )
        <i class="fa fa-times-circle-o fa-2x text-danger"></i>
        @else
        <i class="fa fa-question-circle fa-2x text-warning"></i>
        @endif
    </td>
</tr>
<tr>
    </td>
</tr>
@endforeach

