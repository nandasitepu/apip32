@extends('_l.main') 
@section('title') 
    MEDIA KOMUNIKASI (MAKSI) - APIP SULAWESI BARAT

@endsection
@section('extra')
<style>
    body {
        background: url('img/desa1.jpg') no-repeat center center fixed;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        background-size: cover;
        -o-background-size: cover;
    }
    #transparansi {
        background: url('img/pages/data1.jpg') no-repeat center center fixed;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        background-size: cover;
        -o-background-size: cover;
    }

    .center-btn {
    display: flex;
    align-items: center;
    justify-content: center;
    }

</style>

@endsection

@section('content')
<link href="https://fonts.googleapis.com/css?family=Black+Ops+One" rel="stylesheet">
<div class="col-md-12">
        <div class="hidden-xs">
            <div class="row well center">
                <div class="col-md-2"><img src="/img/sulbar.png" alt="" height="65px"></div>
                <div class="col-md-8" style="font-family: 'Black Ops One', cursive;"> 
                    <i style="font-size:2em; "><span style="color:red;text-decoration:underline">M</span>EDI<span style="color:red;text-decoration:underline">A</span> KOMUNI<span style="color:red;text-decoration:underline">K</span>ASI & INFORMA<span style="color:red;text-decoration:underline">SI</span></i><br>
                    <i style="font-size:2em; ">APIP SULAWESI BARAT</i>
                    
                </div>
                <div class="col-md-2">  <img class="pull-right" src="/img/logo-bpkp.jpg" alt="" height="55px"></div>
            </div>
        </div>
</div>
<div class="row">
    <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-body">
                        @include('_c._slider')
                    </div>
                </div>
                <div class="col-md-12 center">
                        <button class="btn btn-primary btn-sm btn-block center" data-toggle="collapse" data-target="#info">Tentang MAKSI</button>
                    </div>
    
                    <div id="info" class="collapse">
                            <div class="panel panel-default">
                                    <div class="panel-body">
                                        <p>APIP di Provinsi Sulawesi Barat terdiri dari tujuh Inspektorat yang terdiri dari satu Inspektorat Provinsi dan enam Inspektorat Kabupaten, meliputi:</p>
                                        <ol>
                                            <li>Inspektorat Provinsi Sulawesi Barat</li>
                                            <li>Inspektorat Kabupaten Pasangkayu</li>
                                            <li>Inspektorat Kabupaten Mamuju Tengah</li>
                                            <li>Inspektorat Kabupaten Mamuju</li>
                                            <li>Inspektorat Kabupaten Majene</li>
                                            <li>Inspektorat Kabupaten Polewali Mandar</li>
                                            <li>Inspektorat Kabupaten Mamasa</li>
                                        </ol>
                                        <p>Perwakilan BPKP Provinsi Sulawesi Barat telah melakukan kegiatan dalam rangka Peningkatan Kapabilitas APIP level 3, meliputi bimbingan teknis dan Quality Assurance pada semua APIP di Provinsi Sulawesi Barat.</p>
                                        <table class="table table-bordered">
                                            <thead >
                                                <tr >
                                                    <th style="text-align:center">No</th>
                                                    <th style="text-align:center" >APIP</th>
                                                    <th style="text-align:center">Level Kapabilitas APIP</th>
                                                </tr>
                                            </thead>
                                            <tbody style="text-align:center">
                                                <tr>
                                                    <td scope="row">1</td>
                                                    <td>Inspektorat Provinsi Sulawesi Barat</td>
                                                    <td>2+</td>
                                                </tr>
                                                <tr>
                                                    <td scope="row">2</td>
                                                    <td>Inspektorat Kabupaten Pasangkayu</td>
                                                    <td>2+</td>
                                                </tr>
                                                <tr>
                                                    <td scope="row">3</td>
                                                    <td>Inspektorat Kabupaten Mamuju Tengah</td>
                                                    <td>2</td>
                                                </tr>
                                                <tr>
                                                    <td scope="row">4</td>
                                                    <td>Inspektorat Kabupaten Mamuju</td>
                                                    <td>2+</td>
                                                </tr>
                                                <tr>
                                                    <td scope="row">5</td>
                                                    <td>Inspektorat Kabupaten Majene</td>
                                                    <td>2+</td>
                                                </tr>
                                                <tr>
                                                    <td scope="row">6</td>
                                                    <td>Inspektorat Kabupaten Polewali Mandar</td>
                                                    <td>2+</td>
                                                </tr>
                                                <tr>
                                                    <td scope="row">7</td>
                                                    <td>Inspektorat Kabupaten Mamasa</td>
                                                    <td>2+</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                    </div>
                <div class="col-md-4 well text-center">
                <a href="{{route('ppbr')}}">
                        <i class="fa fa-table fa-3x" aria-hidden="true"></i><br>
                        PPBR
                   </a>
                </div>
                <div class="col-md-4 well text-center">
                    <a href="{{route('audit-kinerja')}}">
                        <i class="fa fa-bar-chart fa-3x" aria-hidden="true"></i></i><br>  
                        Audit Kinerja
                    </a>
                </div>
                <div class="col-md-4 well text-center">
                    <a href="{{route('pengembangan-sdm')}}">
                        <i class="fa fa-id-card-o fa-3x" aria-hidden="true"></i> <br>
                        Pengembangan SDM
                    </a>
                    
                </div>
            
               
            </div>
    <div class="col-md-3 text-center">
    <a href="{{route('apip.index')}}"><p class="btn btn-primary btn-xs btn-block" style="font-size:1.5em;">APIP</p></a>
        <div class="panel-body ">
            <a href="{{route('apip.data', 1)}}">
                <div class="btn btn-default btn-block"> Provinsi Sulawesi Barat &nbsp; <img  class="pull-right"src="/img/kab/sulbar.jpg" alt="" height="24px"></div>
            </a>
            
            
        </div>
    
    
        <div class="panel-body">
            <a href="{{route('apip.data', 2)}}">
                <div class="btn btn-default btn-block">Pasangkayu &nbsp; <img  class="pull-right"src="/img/kab/matra.jpg" alt="" height="24px"></div>
            </a>
            
        </div>
    

    
        <div class="panel-body">
            <a href="{{route('apip.data', 3)}}">
                <div class="btn btn-default btn-block">Mamuju Tengah &nbsp; <img  class="pull-right"src="/img/kab/mateng.jpg" alt="" height="24px"></div>
            </a>
        </div>
    
    
        <div class="panel-body">
            <a href="{{route('apip.data', 4)}}">
                <div class="btn btn-default btn-block">Mamuju &nbsp; <img  class="pull-right"src="/img/kab/mamuju.jpg" alt="" height="24px"></div>
            </a>
            
        </div>

    
        <div class="panel-body">
            <a href="{{route('apip.data', 5)}}">
            <div class="btn btn-default btn-block">Majene &nbsp; <img  class="pull-right"src="/img/kab/majene.jpg" alt="" height="24px"></div>
            </a>
            
        </div>
    
    
        <div class="panel-body">
            <a href="{{route('apip.data', 6)}}">
                <div class="btn btn-default btn-block">Polewali Mandar &nbsp; <img  class="pull-right"src="/img/kab/polman.jpg" alt="" height="24px"></div>
            </a>
            
        </div>
    
    
        <div class="panel-body">
            <a href="{{route('apip.data', 7)}}">
                <div class="btn btn-default btn-block"> Mamasa &nbsp; <img  class="pull-right"src="/img/kab/mamasa.jpg" alt="" height="24px"></div> 
            </a>
            
        </div>
                
          
           
           

         
             
            
        
    </div>
   
</div>
@endsection