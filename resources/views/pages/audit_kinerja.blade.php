@extends('_l.main') 
@section('title') 
    MEDIA KOMUNIKASI (MAKSI) - APIP SULAWESI BARAT

@endsection
@section('extra')
<style>
    body {
        background: url('img/desa1.jpg') no-repeat center center fixed;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        background-size: cover;
        -o-background-size: cover;
    }
    #transparansi {
        background: url('img/pages/data1.jpg') no-repeat center center fixed;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        background-size: cover;
        -o-background-size: cover;
    }

    .center-btn {
    display: flex;
    align-items: center;
    justify-content: center;
    }

</style>

@endsection

@section('content')
<div class="row">
    <hr>
    <div class="col-md-12">
        <div class="text-center"><div class="btn btn-primary btn-lg"><b>PEDOMAN AUDIT KINERJA</b></div></div>
        <hr>
    </div>
   
   <div class="col-md-12 center">
        <div class="btn-group-vertical">
            <button type="button" class="btn btn-default btn-lg">Dinas Kesehatan &emsp;<i class="fa fa-file-pdf-o fa-lg" aria-hidden="true"></i></button>
            <button type="button" class="btn btn-default btn-lg">Dinas Pendidikan dan Kebudayaan &emsp;<i class="fa fa-file-pdf-o fa-lg" aria-hidden="true"></i></button>
            <button type="button" class="btn btn-default btn-lg">Dinas PUPR &emsp;<i class="fa fa-file-pdf-o fa-lg" aria-hidden="true"></i></button>
            <button type="button" class="btn btn-default btn-lg">Dinas Penanaman Modal Terpadu Satu Pintu &emsp;<i class="fa fa-file-pdf-o fa-lg" aria-hidden="true"></i></button>
            <button type="button" class="btn btn-default btn-lg">Dinas Sosial &emsp;<i class="fa fa-file-pdf-o fa-lg" aria-hidden="true"></i></button>
            <button type="button" class="btn btn-default btn-lg">Dinas Kelautan dan Perikanan &emsp;<i class="fa fa-file-pdf-o fa-lg" aria-hidden="true"></i></button>
            <button type="button" class="btn btn-default btn-lg">Badan Pendapatan Daerah &emsp;<i class="fa fa-file-pdf-o fa-lg" aria-hidden="true"></i></button>
        </div>
   </div>
   <div class="col-md-12">
    <hr>

   </div>
</div>
@endsection