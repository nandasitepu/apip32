<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Pages
Auth::routes();
Route::get('logout', 'Auth\LoginController@logout')->name('logout' );
Route::match(['get', 'post'], 'register', function(){ return redirect('/'); }); // Disable Registration
Route::get('/', function () {return view('welcome'); })->name('welcome');
Route::get('forum', function () {return view('forum'); })->name('forum');
Route::get('ppbr', function () {return view('pages.ppbr'); })->name('ppbr');
Route::get('audit-kinerja', function () {return view('pages.audit_kinerja'); })->name('audit-kinerja');
Route::get('pengembangan-sdm', function () {return view('pages.pengembangan_sdm'); })->name('pengembangan-sdm');


// APIP
Route::group(['prefix' => 'apip'], function () { 
    Route::get('data/{id}', 'Data\apipCtrl@show_data_apip')->name('apip.data');
    Route::get('data/{id}/detail', 'Data\apipCtrl@show_data_apip_detail')->name('apip.detail');
    Route::get('/', 'Data\apipCtrl@index')->name('apip.index'); 
});


// ADMIN PAGE
Route::group([
    'prefix' => 'dashboard',
    'middleware' => ['role:bpkp_pusat' or 'role:admin'],
], function() {
      // MANAGE USERS
      Route::get('/', function () {return view('dashboard'); })->name('dashboard');
      Route::get('/manage-user', 'ACL\UserController@index')->name('user.index');
      Route::get('/user/{id}/profil', 'ACL\UserController@profil')->name('user.profil');
      Route::put('/user', 'ACL\UserController@update')->name('user.role');
      // MANAGE ROLES
  
      //MANAGE PERMISSION (GATE/POLICY)
  
});








